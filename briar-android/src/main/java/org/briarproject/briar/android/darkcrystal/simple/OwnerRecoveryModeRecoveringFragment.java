package org.briarproject.briar.android.darkcrystal.simple;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.briarproject.briar.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import static java.util.Objects.requireNonNull;

public class OwnerRecoveryModeRecoveringFragment extends SimpleFragment {

	public static final String NUM_RECOVERED = "num_recovered";

	public static final String TAG =
			OwnerRecoveryModeRecoveringFragment.class.getName();
	private int numShards;

	public static OwnerRecoveryModeRecoveringFragment newInstance(
			int numRecovered) {
		Bundle args = new Bundle();
		args.putInt(NUM_RECOVERED, numRecovered);
		OwnerRecoveryModeRecoveringFragment
				fragment = new OwnerRecoveryModeRecoveringFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requireNonNull(getActivity()).setTitle(R.string.title_recovery_mode);

		Bundle args = requireNonNull(getArguments());
		numShards = args.getInt(NUM_RECOVERED);
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater,
			@Nullable ViewGroup container,
			@Nullable Bundle savedInstanceState) {
		View view =
				inflater.inflate(R.layout.fragment_recovery_owner_recovering,
						container, false);

		TextView textViewCount = view.findViewById(R.id.textViewShardCount);
		textViewCount.setText(String.format("%d", numShards));

		return view;
	}

}
